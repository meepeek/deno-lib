export const wrap = (
  fn: (endpoint: string, apiKey: string) => Promise<string>,
) => {
  // deno-lint-ignore no-explicit-any
  return async function (req: any, res: any) {
    let error = "";
    let fn_error = ''
    if (!req.env["ENDPOINT"] || !req.env["API_KEY"]) {
      error +=
        "Environment variables are not set. Function cannot use Appwrite SDK.";
    } else {
      console.log("running function");
      const endpoint = req.env["ENDPOINT"] as string;
      const apiKey = req.env["API_KEY"] as string;

      fn_error = await fn(endpoint, apiKey).catch((e) =>
        error = `catch error on fn call ${JSON.stringify(e)}`
      );
      console.log("done");
    }

    res.json({
      error,
      fn_error
    });
  };
};
