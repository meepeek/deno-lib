import {
  async,
  datetime,
  family_fund_2022,
  scraper,
  sdk,
} from "./deps.ts";
import { Financial } from "../_type.ts";

export async function loop_thru_appwrite_docs(
  endpoint: string,
  apiKey: string,
  project_name: string,
  collection_name: string,
  // deno-lint-ignore no-explicit-any
  fn: (doc: any) => Promise<any> | any,
  appwrite_query = [] as string[],
  limit = 1000 // max docs to loop thru
) {
  const workers_limit = new async.Semaphore(3);
  const project = (() => {
    const client = new sdk.Client();
    client
      .setEndpoint(endpoint) // Your API Endpoint
      .setProject(project_name) // Your project ID
      .setKey(apiKey); // Your secret API key
    const project = new sdk.Database(client);
    return project;
  })();

  const outputs = []
  let count = 0
  let cursor = "";
  do {
    const { documents } = await project.listDocuments(
      collection_name,
      appwrite_query,
      100,
      0,
      cursor,
    );
    const workers = documents.map(async ({ $id }) => {
      const doc = await project.getDocument(collection_name, $id);
      await workers_limit.with(async () => await fn(doc));
    });
    const results = await Promise.allSettled(workers);
    outputs.push(...results)
    count += documents.length
    cursor = documents.length > 0 ? documents.slice(-1)[0].$id : "";
  } while (cursor && limit === null ? true : (count < limit));

  return outputs
}

export async function get_symbol_info_by_id(
  endpoint: string,
  apiKey: string,
  symbol_info_id: string,
) {
  const data_collector = (() => {
    const client = new sdk.Client();
    client
      .setEndpoint(endpoint) // Your API Endpoint
      .setProject("data_collector") // Your project ID
      .setKey(apiKey); // Your secret API key
    const project = new sdk.Database(client);
    return project;
  })();

  const result = await data_collector.listDocuments(
    "symbol_info",
    [sdk.Query.equal("$id", symbol_info_id)],
    1,
  );
  if (result.total === 0) return null;
  const symbol_info = (() =>
    result.documents[0] as {
      // deno-lint-ignore no-explicit-any
      [key: string]: any;
    })() as family_fund_2022.SymbolInfo;
  return symbol_info;
}
export async function get_symbol_info(
  endpoint: string,
  apiKey: string,
  symbol: string,
  type?: string,
  market?: string,
) {
  const data_collector = (() => {
    const client = new sdk.Client();
    client
      .setEndpoint(endpoint) // Your API Endpoint
      .setProject("data_collector") // Your project ID
      .setKey(apiKey); // Your secret API key
    const project = new sdk.Database(client);
    return project;
  })();

  const appwrite_query = (() => {
    const output = [sdk.Query.equal("symbol", symbol)];
    if (type) output.push(sdk.Query.equal("type", type));
    if (market) output.push(sdk.Query.equal("market", market));
    return output;
  })();
  const result = await data_collector.listDocuments(
    "symbol_info",
    appwrite_query,
  );
  if (result.total === 0) return null;
  const symbol_info = (() =>
    result.documents[0] as {
      // deno-lint-ignore no-explicit-any
      [key: string]: any;
    })() as family_fund_2022.SymbolInfo;
  return symbol_info;
}
export async function get_close_price_by_id(
  endpoint: string,
  apiKey: string,
  symbol_info_id: string,
  from?: string,
) {
  const symbol_info = await get_symbol_info_by_id(
    endpoint,
    apiKey,
    symbol_info_id,
  );
  if (!symbol_info) return [];
  return await get_close_price_by_symbol_info(symbol_info, from);
}
export async function get_close_price_by_symbol(
  endpoint: string,
  apiKey: string,
  symbol: string,
  type?: string,
  market?: string,
  from?: string,
) {
  const symbol_info = await get_symbol_info(
    endpoint,
    apiKey,
    symbol,
    type,
    market,
  );
  if (!symbol_info) return [];
  return await get_close_price_by_symbol_info(symbol_info, from);
}
export async function get_finance_by_id(
  endpoint: string,
  apiKey: string,
  symbol_info_id: string,
) {
  const data_collector = (() => {
    const client = new sdk.Client();
    client
      .setEndpoint(endpoint) // Your API Endpoint
      .setProject("data_collector") // Your project ID
      .setKey(apiKey); // Your secret API key
    const project = new sdk.Database(client);
    return project;
  })();

  const financial_data_ids = await (async () => {
    let cursor = "";
    const output = [];
    do {
      const respond = await data_collector.listDocuments(
        "financial_data",
        [
          sdk.Query.equal("symbol_info_id", symbol_info_id),
        ],
        100,
        0,
        cursor,
      );
      const financial_data_ids = respond.documents.map((o) => o.$id);
      output.push(...financial_data_ids);
      cursor = respond.total === 0 ? respond.documents.slice(-1)[0].$id : "";
    } while (cursor);
    return output;
  })();

  const finances = await (async () => {
    const promises = financial_data_ids.map(async (financial_data_id) =>
      await data_collector.getDocument("financial_data", financial_data_id)
    );
    const results = await Promise.allSettled(promises);
    // deno-lint-ignore no-explicit-any
    return results.map((o: { [key: string]: any }) => o.value);
  })();
  return finances as Financial[];
}
export async function get_finance_by_symbol(
  endpoint: string,
  apiKey: string,
  symbol: string,
  type?: string,
  market?: string,
) {
  const symbol_info = await get_symbol_info(
    endpoint,
    apiKey,
    symbol,
    type,
    market,
  );
  if (!symbol_info || !symbol_info.$id) return null;
  return get_finance_by_id(endpoint, apiKey, symbol_info.$id);
}
export async function get_last_price_by_id(
  endpoint: string,
  apiKey: string,
  symbol_info_id: string,
) {
  const symbol_info = await get_symbol_info_by_id(
    endpoint,
    apiKey,
    symbol_info_id,
  );
  if (!symbol_info) return null;
  return await get_last_price_by_symbol_info(symbol_info);
}
export async function get_last_price_by_symbol(
  endpoint: string,
  apiKey: string,
  symbol: string,
  type?: string,
  market?: string,
) {
  const symbol_info = await get_symbol_info(
    endpoint,
    apiKey,
    symbol,
    type,
    market,
  );
  if (!symbol_info) return null;
  return await get_last_price_by_symbol_info(symbol_info);
}

// sub functions
async function get_last_price_by_symbol_info(
  symbol_info: family_fund_2022.SymbolInfo,
) {
  if (symbol_info.market === "Thailand") {
    const price = await scraper.set_or_th.getCurrentPrice(symbol_info.symbol);
    const current_price = price ? price.price : await (async () => {
      const prices = await scraper.set_or_th.getClosePrices(symbol_info.symbol);
      return prices.slice(-1)[0].close;
    })();
    return current_price;
  } else {
    const quote = await scraper.investing_com.getQuote(
      symbol_info.symbol,
      symbol_info.market,
    );
    return quote.lp;
  }
}
async function get_close_price_by_symbol_info(
  symbol_info: family_fund_2022.SymbolInfo,
  from = datetime.format(
    (() => {
      // default to 14 days prior
      const temp = new Date();
      temp.setDate(temp.getDate() - 14);
      return temp;
    })(),
    "yyyy-MM-dd",
  ),
  to = datetime.format(new Date(), "yyyy-MM-dd"),
) {
  const { symbol, type } = symbol_info;
  if (symbol_info.cache) {
    console.log("cache not yet implement");
    return [];
  } else if (symbol_info.finnomena_id) {
    const result = await scraper.finnomena_com.getClosePrices(
      symbol,
      type,
      from,
      to,
    );
    const prices = result.map((o) => {
      const {
        date,
        o: open,
        c: close,
        h: high,
        l: low,
        v: volume,
      } = o;
      return { date, open, close, high, low, volume };
    });
    return prices;
  } else if (symbol_info.pair_id) {
    const result = await scraper.investing_com.getClosePrices(
      symbol_info.pair_id,
      from,
      to,
    );
    const prices = result.map((o) => {
      const {
        date,
        o: open,
        c: close,
        h: high,
        l: low,
        v: volume,
      } = o;
      return { date, open, close, high, low, volume };
    });
    return prices;
  } else return [];
}
