import { get_finance_by_id } from "../_.ts";
import { apiKey, endpoint } from "../../credentials/appwrite.ts";

if (import.meta.main) await test();
async function test() {
  const symbol_info_id = Deno.args[0] ? Deno.args[0].toUpperCase() : "62af8658efd5ad1b456b";

  const result = await get_finance_by_id(endpoint, apiKey, symbol_info_id).catch((e) => console.log(e));
  console.log(result);
}
