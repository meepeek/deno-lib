import {loop_thru_appwrite_docs} from '../_.ts'
import { apiKey, endpoint } from "../../credentials/appwrite.ts";
import {family_fund_2022} from '../deps.ts'

const results = await loop_thru_appwrite_docs(endpoint, apiKey, 'data_collector', 'symbol_info', fn, undefined, null)
console.log(results)

function fn(doc: family_fund_2022.SymbolInfo) {
    console.log(doc.$id, doc.symbol, doc.name_en)    
}