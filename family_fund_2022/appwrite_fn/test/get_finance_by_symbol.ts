import { get_finance_by_symbol } from "../_.ts";
import { apiKey, endpoint } from "../../credentials/appwrite.ts";

if (import.meta.main) await test();
async function test() {
  const symbol = Deno.args[0] ? Deno.args[0].toUpperCase() : "AP";
  const type = Deno.args[1] ? Deno.args[1].toUpperCase() : undefined;
  const market = Deno.args[2] ? Deno.args[2].toUpperCase() : undefined;

  const result = await get_finance_by_symbol(
    endpoint,
    apiKey,
    symbol,
    type,
    market,
  ).catch((e) => console.log(e));
  console.log(result);
}
