export type CronQueue = {
    tag: string,
    params: string,
    status: 'queue' | 'done' | 'error',
    $id: string
}

export type Financial = {
    symbol_info_id: string,
    fiscal: number,
    quarter: number,
    net_profit: number,
    rps?: number,
    eps?: number,
    dps?: number,
    revenue?: number,
    sga?: number,
    da?: number,
    gpm?: number,
    npm?: number,
    equity?: number,
    roe?: number,
    roa?: number,
    de?: number,
    cap_expenditure_per_share?: number,
    book_value_per_share?: number,
    common_shares_outstanding?: number,
    gross_profit?: number,
    operating_income?: number,
    operating_margin?: number,
    sga_per_revenue?: number,
    tax_rate?: number,
    working_capital?: number,
    long_term_debt?: number,
    current_ratio?: number,
    dividend_payout_ratio?: number,
    cash_conversion_cycle?: number,
    operating_activities?: number,
    investing_activities?: number,
    financing_activities?: number,
    net_change_in_cash?: number
}

export type ClosePrice = {
    symbol_info_id: string,
    date: string,
    open?: number,
    close: number,
    high?: number,
    low?: number,
    volume: number,
    value?: number,
    avg?: number,
    chg?: number,
    chg_p?: number,
    total_share?: number,
    par?: number,
    nav?: number
}

export type SymbolInfo = {
    $id?: string;
    symbol: string;
    type: string;
    market: string;
    sector?: string;
    name_th?: string;
    name_en: string;
    timezone: string;
    pair_id?: number;
    finnomena_id?: string;
    cache?: boolean;
}