import { denodom } from "../deps.ts";
import { FinanceInfoFormat, MarketDict } from "./_type.ts";

interface FinI {
  [x: string]: number | null;
}
interface FinRawI {
  [x: string]: string | null;
}

/**
 * @param symbol
 * @param priceData priceData instance for query price
 * @returns
 */
export async function getFinanceData(
  symbol: string,
  market: string,
  cookie =
    "ajs_anonymous_id=%2245f9fe8b-d0de-464d-8cce-71934332439b%22; _ga=GA1.2.465064915.1647543244; _fbp=fb.1.1647543244339.2104770809; hunt7586-_zldp=ss%2BYrC93NjO1BAxgDcfayRyePwMuUvkn%2FAjVu4NTEsQ%2BqhaqdvbZk8M0qCNCorCjUWfBQUDgTVw%3D; jitta_nu=2; ajs_user_id=%225fd50c6ac251a3001a167af9%22; _gac_UA-135483057-1=1.1647590944.Cj0KCQjw29CRBhCUARIsAOboZbLabd8OdJ3BPdwuQYwXmrAzMhAVLkT6ZpAqE7Ec6URnQn94oa6quSYaAgfCEALw_wcB; _gac_UA-148344773-2=1.1647590944.Cj0KCQjw29CRBhCUARIsAOboZbLabd8OdJ3BPdwuQYwXmrAzMhAVLkT6ZpAqE7Ec6URnQn94oa6quSYaAgfCEALw_wcB; gclid=Cj0KCQjw29CRBhCUARIsAOboZbLabd8OdJ3BPdwuQYwXmrAzMhAVLkT6ZpAqE7Ec6URnQn94oa6quSYaAgfCEALw_wcB; _gid=GA1.2.4165617.1649319575; _hjSessionUser_1187728=eyJpZCI6ImIyZDBkOThiLWRlYTktNWUzZi04MWZkLTBjMjY4ODcwMjlhYyIsImNyZWF0ZWQiOjE2NDc1NDM0MjA1MDAsImV4aXN0aW5nIjp0cnVlfQ==; hunt7586-_zldt=1936e3d6-1fac-4754-8c66-58e68b12ee98-0; JDCID=2f220c02083a65fa0c4713b13964b6fa369aefe9a0660bc99dafea0d9c3f844d.5439c914472955e6ecc4ac7199a2a5c2dafd535ac3d93df6e1b632789a69046d22012e157f5bbd49210a4b2cc4dec3851703061b2513e31a45162b61f84810a360b432eb405ca209b920a0f8f433b0620c295848890c; mp_d55123f7330a45efc28a32923e676d48_mixpanel=%7B%22distinct_id%22%3A%20%225fd50c6ac251a3001a167af9%22%2C%22%24device_id%22%3A%20%2217f993b787617-0a9be71aa575fe-192b1e05-100200-17f993b7877759%22%2C%22%24initial_referrer%22%3A%20%22https%3A%2F%2Faccounts.jitta.com%2F%22%2C%22%24initial_referring_domain%22%3A%20%22accounts.jitta.com%22%2C%22%24user_id%22%3A%20%225fd50c6ac251a3001a167af9%22%2C%22%24search_engine%22%3A%20%22google%22%2C%22utm_campaign%22%3A%20%22sem%22%7D; hunt7586-nextrecon=1649325588270; zld404185000000002043state=0",
): Promise<FinanceInfoFormat[]> {
  const mkt = MarketDict[market]
  if (!mkt) return [];
  const url = `https://www.jitta.com/stock/${encodeURIComponent(mkt)}:${
    encodeURIComponent(symbol)
  }/factsheet`;
  const options = {
    method: "GET",
    headers: {
      "accept":
        "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
      "accept-language": "en-US,en;q=0.9",
      "cache-control": "max-age=0",
      "sec-ch-ua":
        '" Not A;Brand";v="99", "Chromium";v="99", "Google Chrome";v="99"',
      "sec-ch-ua-mobile": "?0",
      "sec-ch-ua-platform": '"Linux"',
      "sec-fetch-dest": "document",
      "sec-fetch-mode": "navigate",
      "sec-fetch-site": "same-origin",
      "sec-fetch-user": "?1",
      "upgrade-insecure-requests": "1",
      "cookie": cookie,
      "Referer": "https://accounts.jitta.com/",
      "Referrer-Policy": "strict-origin-when-cross-origin",
    },
  };
  const response = await fetch(url, options);
  const data = await response.text();

  const document = new denodom.DOMParser().parseFromString(data, "text/html");

  const quarterlyTable = document?.querySelector(
    "#app > div > div.Layout__Content-sc-194vx9d-1.ghuUYV > div > div > div > div.withFooter__ContentWrapper-hhd5n3-1.fKnzex > div.withFooter__ComponentWrapper-hhd5n3-0.bqerfn > div > div > div > div:nth-child(1) > div > div > div.Factsheet__FactsheetTableContainerStyles-zd9e05-12.iErwyV > div > div",
  );

  const finances = quarterlyTable
    ? await financeTableExtract(quarterlyTable)
    : [];

  return finances as FinanceInfoFormat[];
}

function financeTableExtract(table: denodom.Element) {
  const output: { [x: string]: number | null }[] = [];

  // parse header
  const rawHeaders = [
    ...table.querySelectorAll(
      ".Text__TextSM-dn2wcp-2.FactsheetTableHeader__FlexItem-efsh34-3",
    ),
  ] as denodom.Element[];
  for (const col of rawHeaders) {
    const [fiscal, quarter] = col.textContent.split("-");
    output.push({
      fiscal: Number(fiscal),
      quarter: quarter ? Number(quarter) : 9,
    });
  }

  // parse data
  const dataRows = [
    ...table.querySelectorAll(".FactsheetTableRow__FlexContainer-sc-1wziwtk-1"),
  ] as denodom.Element[];

  for (const row of dataRows) {
    const columns = [
      ...row.querySelectorAll(".Text__TextSM-dn2wcp-2"),
    ] as denodom.Element[];
    const key = translatePropName(
      columns.slice(-1)[0].textContent.replaceAll("?", ""),
    );
    const values = columns.slice(0, -2).map((column) => {
      const text = column.textContent.replaceAll(",", "");
      if (text === "-") return null;
      const suffix = text.slice(-1);
      if (suffix === "%") return percent();
      if (suffix === "M") return million();
      return general();

      function percent() {
        return Number(text.slice(0, -1));
      }
      function million() {
        return 1000000 * Number(text.slice(0, -1));
      }
      function general() {
        return Number(text);
      }
    });
    values.map((v, i) => key ? output[i][key] = v : null);
  }

  // fill price
  // const jobs = output.map( o => async () => {
  //     const {fiscal} = o
  //     const from = `${fiscal}-12-21`
  //     const to = `${fiscal}-12-31`
  //     const {data: prices} = await priceData.getPrice({symbol, type: 'stock'}, from, to)
  //     if (prices.length === 0) o.close = null
  //     else {
  //         const close = prices.slice(-1)[0].price
  //         o.close = Number(close)
  //     }
  // } )
  // for (const i in jobs) await jobs[i]()

  // // calculate totalShare
  // output.map(o => o.totalShare = (o.netProfit && o.eps) ? o.netProfit/o.eps : null)

  // // calculate pe
  // output.map(o => (!o.close || !o.eps || o.eps <= 0) ? o.pe = null : o.pe = o.close/o.eps)

  // // calculate yield
  // output.map(o => (!o.close || !o.dps) ? o.dividendYield = null : o.dividendYield = 100 * o.dps/o.close)

  // clear unavailable year data where data = - -
  const cleanOutput = output.reduce((a: FinI[], o) => {
    if (o.netProfit) a.push(o);
    return a;
  }, []);

  return cleanOutput;
}

function translatePropName(name: string) {
  const lookupTable: FinRawI = {
    รายได้ต่อหุ้น: "rps",
    กำไรต่อหุ้น: "eps",
    เงินปันผลต่อหุ้น: "dps",
    ค่าใช้จ่ายในการลงทุนต่อหุ้น: "capExpenditurePerShare",
    มูลค่าตามบัญชีต่อหุ้น: "bookValuePerShare",
    จำนวนหุ้นสามัญที่เรียกชำระแล้ว: "commonSharesOutstanding",
    รายได้รวม: "revenue",
    กำไรขั้นต้น: "grossProfit",
    ค่าใช้จ่ายในการขายและบริหาร: "sga",
    ค่าเสื่อมราคาและค่าตัดจำหน่าย: "da",
    รายได้จากการดำเนินงาน: "operatingIncome",
    กำไรสุทธิ: "netProfit",
    อัตรากำไรขั้นต้น: "gpm",
    อัตรากำไรจากการดำเนินงาน: "operatingMargin",
    อัตรากำไรสุทธิ: "npm",
    ค่าใช้จ่ายในการขายและบริหารต่อยอดขาย: "sgaPerRevenue",
    อัตราภาษี: "taxRate",
    เงินทุนหมุนเวียน: "workingCapital",
    หนี้สินระยะยาว: "longTermDebt",
    ส่วนของผู้ถือหุ้น: "equity",
    อัตราผลตอบแทนผู้ถือหุ้น: "roe",
    อัตราผลตอบแทนจากสินทรัพย์: "roa",
    อัตราส่วนสภาพคล่อง: "currentRatio",
    หนี้สินต่อส่วนของผู้ถือหุ้น: "de",
    อัตราการจ่ายเงินปันผล: "dividendPayoutRatio",
    วงจรเงินสด: "cashConversionCycle",
    เงินสดจากกิจกรรมดำเนินงาน: "operatingActivities",
    เงินสดจากกิจกรรมลงทุน: "investingActivities",
    กระแสเงินสดที่ได้จากการจัดหาเงิน: "financingActivities",
    เงินสดเปลี่ยนแปลงสุทธิ: "netChangeInCash",
  };
  return lookupTable[name];
}

if (import.meta.main) await main();
async function main() {
  const symbol = Deno.args[0] ? Deno.args[0].toUpperCase() : "AP";
  const market = Deno.args[1] ? Deno.args[1].toUpperCase() : "Thailand";
  const result = await getFinanceData(symbol, market);
  console.log(result);
}
