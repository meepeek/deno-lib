export type FinanceInfoFormat = {
    fiscal: number,
    quarter: number,
    netProfit: number,
    rps?: number,
    eps?: number,
    dps?: number,
    revenue?: number,
    sga?: number,
    da?: number,
    gpm?: number,
    npm?: number,
    equity?: number,
    roe?: number,
    roa?: number,
    de?: number,
    capExpenditurePerShare?: number,
    bookValuePerShare?: number,
    commonSharesOutstanding?: number,
    grossProfit?: number,
    operatingIncome?: number,
    operatingMargin?: number,
    sgaPerRevenue?: number,
    taxRate?: number,
    workingCapital?: number,
    longTermDebt?: number,
    currentRatio?: number,
    dividendPayoutRatio?: number,
    cashConversionCycle?: number,
    operatingActivities?: number,
    investingActivities?: number,
    financingActivities?: number,
    netChangeInCash?: number
}

export const MarketDict: Record<string, string> = {
    Thailand: 'bkk',
    NYSE: 'nyse',
    NASDAQ: 'nasdaq',
    'Hong Kong': 'hkg',
    Shenzhen: 'chn',
    Shanghai: 'chn',
    Taiwan: 'twn',
    Sydney: 'aus',
    Seoul: 'kor',
    Paris: 'fra',
    BSE: 'ind', // india
    London: 'lon',
    Xetra: 'deu', // german
    Switzerland: 'che',
    Singapore: 'sgx',
    Moscow: 'rus', // russia
    Philippines: 'phl',
    Amsterdam: 'nld', // netherlands
    'Kuala Lumpur': 'mal', // malaysia
    Tokyo: 'tyo', // japan
    Milan: 'ita', // italy
    Jakarta: 'idn', // indonesia
    'BM&FBovespa': 'bra', // brazil
    DSE: 'bgd', // bangladesh
    'Ho Chi Minh': 'vnm', // vietnam
    'Abu Dhabi': 'are', // arab
    Dubai: 'are', // arab
    Stockholm: 'swe', // sweden
}