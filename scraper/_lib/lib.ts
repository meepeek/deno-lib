/**
 * convert date object to unix timestamp
 * @param date Date Object
 * @returns 
 */
 export function convertDateToUnix(date: Date) {
    return Math.floor( date.getTime() / 1000 )
}