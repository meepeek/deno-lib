import {StockQuoteFormat} from './_type.ts'

export async function getQuote(
  symbol: string,
  exchange: string,
  sid = "8a7e26dbed121c6a9ef5585f1a848e37/1654835641/1/1/8",
) {
  const url =
    `https://tvc6.investing.com/${sid}/quotes?symbols=${exchange}%20%3A${symbol}`;
  const response = await fetch(url);
  const {d} = await response.json();

  return d[0].v as StockQuoteFormat;
}

if (import.meta.main) await main();
async function main() {
  const symbol = Deno.args[0] ? Deno.args[0] : 'AAPL';
  const market = Deno.args[1] ? Deno.args[1] : 'NASDAQ';

  const result = await getQuote(symbol, market);
  console.log(result);
}
