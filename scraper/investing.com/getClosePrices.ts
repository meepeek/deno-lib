import {convertDateToUnix} from '../_lib/lib.ts'
import {ClosePriceFormat} from './_type.ts'

/**
 * 
 * @param pairId investing.com pairId
 * @param from YYYY-MM-DD
 * @param to YYYY-MM-DD
 * @returns [{date, o, h, c, l, v}]
 */
export async function getClosePrices(pairId: number, from: string|null = null, to: string|null = null) {
    // default values in case of null
    from = from ? from : (() => {
        const temp = new Date()
        temp.setDate(temp.getDate() - 14)
        return temp.toISOString().split('T')[0]
    })()
    to = to ? to : (new Date()).toISOString().split('T')[0]

    const unixFrom = convertDateToUnix(new Date(from))
    const unixTo = convertDateToUnix(new Date(to))

    const url = `https://tvc6.forexpros.com/caac0fffa21064bae36902ed1264197c/1623816682/1/1/8/history?symbol=${pairId}&resolution=D&from=${unixFrom}&to=${unixTo}`
    const response = await fetch(url)
    const data = await response.json()

    return parser(data) as ClosePriceFormat[]

    function parser(data: { [key: string]: number[] }) {
        const {t, c, h, l, o, v} = data
        return t.map( (unix, i) => {
            const date = new Date(unix * 1e3).toISOString().split('T')[0]
            return {
                date,
                o: o[i],
                c: c[i],
                h: h[i],
                l: l[i],
                v: v[i]
            }
        } )
    }
}

if (import.meta.main) await main()
async function main() {
    const pairId = Deno.args[0] ? Number(Deno.args[0]) : 6408
    const result = await getClosePrices(pairId)
    console.log(result)
}
