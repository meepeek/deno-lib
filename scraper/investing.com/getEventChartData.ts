import {EventChartData} from './_type.ts'

export async function getEventChartData(
  eventCode: number,
) {
  const url =
    `https://sbcharts.investing.com/events_charts/us/${eventCode}.json`;
  const response = await fetch(url);
  const data = await response.json();

  return data.attr as EventChartData[];
}

if (import.meta.main) await main();
async function main() {
  const eventCode = Deno.args[0] ? Number(Deno.args[0]) : 478;

  const result = await getEventChartData(eventCode);
  console.log(result);
}
