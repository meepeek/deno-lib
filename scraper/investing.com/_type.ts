export enum EventCodeMap {
    'thai-interest' = 478,
    'us-interest' = 168,
    'europe-interest' = 164,
    'china-interest' = 1083,
    'thai-core-cpi' = 1225,
    'thai-cpi' = 800,
    'us-core-cpi' = 56,
    'us-cpi' = 69,
    'europe-core-cpi' = 317,
    'europe-cpi' = 68,
    'china-cpi' = 459
}

export type EventChartData = {
  timestamp: number,
  actual_state: string,
  actual: number,
  actual_formatted: string,
  forecast: number | null,
  forecast_formatted: string,
  revised: number | null,
  revised_formatted: string
}

export type SymbolInfoFormat = {
    name: string,
    "exchange-traded": string,
    "exchange-listed": string,
    timezone: string,
    minmov: number,
    minmov2: number,
    pricescale: number,
    pointvalue: number,
    has_intraday: boolean,
    has_no_volume: boolean,
    volume_precision: number,
    ticker: string,
    description: string,
    type: string,
    has_daily: boolean,
    has_weekly_and_monthly: boolean,
    supported_resolutions: string[],
    intraday_multipliers: string[],
    session: string,
    data_status: string
};

export type StockQuoteFormat = {
    ch: string,
    chp: string,
    short_name: string,
    exchange: string,
    description: string,
    lp: string,
    ask: string,
    bid: string,
    spread: number,
    open_price: string,
    high_price: string,
    low_price: string,
    prev_close_price: string,
    volume: string
};

export type ClosePriceFormat = {
    date: string,
    o: number,
    c: number,
    h: number,
    l: number,
    v: number
}