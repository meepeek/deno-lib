import { async, denodom } from "../deps.ts";

export async function getPairIdList() {
    const pairIds: number[] = [await getCryptoPagePairId(), await getStockPairIds(), await getCommonPagePairId()].flat()

    return pairIds;
}

if (import.meta.main) await main();
async function main() {
  const result = await getPairIdList();
  console.log(result);
}

async function getCryptoPagePairId() {
    const url = 'https://www.investing.com/crypto/currencies'
    const response = await fetch(url)
    const data = await response.text()

    const document = new denodom.DOMParser().parseFromString(data, 'text/html')
    const allIdsInPage = document ? extractPairIdFromCryptoPage() : []
    const pairIds = [...new Set(allIdsInPage)]

    return pairIds as number[]

    function extractPairIdFromCryptoPage() {
        const elms = document ? [...document.querySelectorAll("a[class^='pid-']")] as denodom.Element[] : []
        const pairIds = elms ? [...elms].map(elm => {
            const s = elm.getAttribute('class')?.toString()
            const pairId = s ? Number(s.split('-')[1]) : null
            return pairId
        }).filter(v => v !== null) : []
        return pairIds
    }
}

async function getCommonPagePairId() {
    const urls = [
        {name: 'index-page', url: 'https://www.investing.com/indices/world-indices?'},
        {name: 'commodity-page', url: 'https://www.investing.com/commodities/'},
        {name: 'fund-page', url: 'https://www.investing.com/funds/world-funds?&issuer_filter=0'},
        {name: 'currency-page', url: 'https://www.investing.com/currencies/single-currency-crosses'},
        {name: 'etf-page', url: 'https://www.investing.com/etfs/world-etfs?&issuer_filter=0'},
        {name: 'bonds-page', url: 'https://www.investing.com/rates-bonds/world-government-bonds?maturity_from=10&maturity_to=310'}
    ]

    const workers = urls.map(async ({name, url}) => {
        console.log(`working on ${name}`)
        const response = await fetch(url)
        const data = await response.text()
    
        const document = new denodom.DOMParser().parseFromString(data, 'text/html')
        const allIdsInPage = document ? extractPairIdFromPage(document) : []
        const pairIds = [...new Set(allIdsInPage)]
    
        return pairIds    
    })

    const results = await Promise.all(workers)
    const pairIds = results.flat()

    return pairIds as number[]
}

async function getStockPairIds() {
    const pages = [
        { id: 1084862, name: "Investing.com United States 30", type: "stock-page" },
        { id: 169, name: "Dow Jones Industrial Average", type: "stock-page" },
        { id: 20, name: "Nasdaq 100", type: "stock-page" },
        { id: 14958, name: "NASDAQ Composite", type: "stock-page" },
        { id: 166, name: "S&P 500", type: "stock-page" },
        { id: 24441, name: "S&P/TSX Composite", type: "stock-page" },
        { id: 17920, name: "Bovespa", type: "stock-page" },
        { id: 27254, name: "S&P/BMV IPC", type: "stock-page" },
        { id: 13376, name: "S&P Merval", type: "stock-page" },
        { id: 49642, name: "COLCAP", type: "stock-page" },
        { id: 14767, name: "S&P CLX IPSA", type: "stock-page" },
        { id: 49680, name: "S&P Lima General", type: "stock-page" },
        { id: 49748, name: "Bursatil", type: "stock-page" },
        { id: 172, name: "DAX", type: "stock-page" },
        { id: 175, name: "Euro Stoxx 50", type: "stock-page" },
        { id: 27, name: "FTSE 100", type: "stock-page" },
        { id: 167, name: "CAC 40", type: "stock-page" },
        { id: 174, name: "IBEX 35", type: "stock-page" },
        { id: 176, name: "SMI", type: "stock-page" },
        { id: 168, name: "AEX", type: "stock-page" },
        { id: 1016254, name: "ATX", type: "stock-page" },
        { id: 25685, name: "OMX Stockholm 30", type: "stock-page" },
        { id: 25760, name: "OMX Copenhagen 20", type: "stock-page" },
        { id: 25823, name: "OMX Helsinki 25", type: "stock-page" },
        { id: 25887, name: "ICEX Main", type: "stock-page" },
        { id: 14603, name: "OSE Benchmark", type: "stock-page" },
        { id: 44400, name: "Oslo OBX", type: "stock-page" },
        { id: 14601, name: "BEL 20", type: "stock-page" },
        { id: 14600, name: "PSI", type: "stock-page" },
        { id: 14602, name: "WIG20", type: "stock-page" },
        { id: 41044, name: "WIG30", type: "stock-page" },
        { id: 38014, name: "Budapest SE", type: "stock-page" },
        { id: 14774, name: "CROBEX", type: "stock-page" },
        { id: 49753, name: "Cyprus Main Market", type: "stock-page" },
        { id: 50647, name: "PX", type: "stock-page" },
        { id: 961613, name: "Athens General Composite", type: "stock-page" },
        { id: 49629, name: "BSE SOFIX", type: "stock-page" },
        { id: 14770, name: "BET", type: "stock-page" },
        { id: 49648, name: "ISEQ Overall", type: "stock-page" },
        { id: 41155, name: "SAX", type: "stock-page" },
        { id: 29149, name: "Blue-Chip SBITOP", type: "stock-page" },
        { id: 945510, name: "Belex 15", type: "stock-page" },
        { id: 945507, name: "BIRS", type: "stock-page" },
        { id: 945505, name: "Sarajevo 10", type: "stock-page" },
        { id: 13666, name: "MOEX Russia", type: "stock-page" },
        { id: 13665, name: "RTSI", type: "stock-page" },
        { id: 29191, name: "PFTS", type: "stock-page" },
        { id: 19155, name: "BIST 100", type: "stock-page" },
        { id: 10529, name: "TA 35", type: "stock-page" },
        { id: 178, name: "Nikkei 225", type: "stock-page" },
        { id: 39929, name: "BSE Sensex 30", type: "stock-page" },
        { id: 17940, name: "Nifty 50", type: "stock-page" },
        { id: 179, name: "Hang Seng", type: "stock-page" },
        { id: 40820, name: "Shanghai Composite", type: "stock-page" },
        { id: 942630, name: "SZSE Component", type: "stock-page" },
        { id: 171, name: "S&P/ASX 200", type: "stock-page" },
        { id: 41146, name: "NZX 50", type: "stock-page" },
        { id: 41148, name: "NZX MidCap", type: "stock-page" },
        { id: 37426, name: "KOSPI", type: "stock-page" },
        { id: 49661, name: "KOSPI 50", type: "stock-page" },
        { id: 38015, name: "SET Index", type: "stock-page" },
        { id: 29151, name: "CSE All-Share", type: "stock-page" },
        { id: 38017, name: "Taiwan Weighted", type: "stock-page" },
        { id: 11319, name: "Tadawul All Share", type: "stock-page" },
        { id: 49692, name: "PSEi Composite", type: "stock-page" },
        { id: 13185, name: "QE General", type: "stock-page" },
        { id: 13009, name: "Al-Quds", type: "stock-page" },
        { id: 49677, name: "Karachi 100", type: "stock-page" },
        { id: 13384, name: "MSM 30", type: "stock-page" },
        { id: 13518, name: "Bahrain All Share", type: "stock-page" },
        {
          id: 29049,
          name: "Jakarta Stock Exchange Composite Index",
          type: "stock-page",
        },
        { id: 995072, name: "HNX 30", type: "stock-page" },
        { id: 41064, name: "VN 30", type: "stock-page" },
        { id: 41063, name: "VN Index", type: "stock-page" },
        { id: 12259, name: "Amman SE General", type: "stock-page" },
        { id: 12522, name: "DFM General", type: "stock-page" },
        { id: 941336, name: "FTSE ADX General", type: "stock-page" },
        { id: 14523, name: "BLOM Stock", type: "stock-page" },
        { id: 29078, name: "FTSE Malaysia KLCI", type: "stock-page" },
        { id: 12860, name: "EGX 30", type: "stock-page" },
        { id: 1159121, name: "EGX 70 EWI ", type: "stock-page" },
        { id: 41043, name: "South Africa Top 40", type: "stock-page" },
        { id: 18823, name: "Tunindex", type: "stock-page" },
        { id: 948432, name: "Tunindex20", type: "stock-page" },
        { id: 13228, name: "Moroccan All Shares", type: "stock-page" },
        { id: 41159, name: "Semdex", type: "stock-page" },
        { id: 41115, name: "BSE Domestic Company", type: "stock-page" },
        { id: 41164, name: "Tanzania All Share", type: "stock-page" },
        { id: 41111, name: "Uganda All Share", type: "stock-page" },
        { id: 101797, name: "NSE 30", type: "stock-page" },
        { id: 980229, name: "BRVM 10", type: "stock-page" },
        { id: 993158, name: "ISX Main 60", type: "stock-page" },
        { id: 989440, name: "Dhaka Stock Exchange 30", type: "stock-page" },
        { id: 993152, name: "KASE", type: "stock-page" },
        { id: 1006456, name: "MNSE 10", type: "stock-page" },
        { id: 1006455, name: "MNE Top 20", type: "stock-page" },
      ];
      
    const output = new Set()
    const workersLimit = new async.Semaphore(1)
    const workers = pages.map( async ({name, id}) => {
        await workersLimit.with( async () => {
            console.log(`working on ${name}`)
            const url = `https://www.investing.com/equities/StocksFilter?noconstruct=1&smlID=0&sid=&tabletype=price&index_id=${id}`
            const response = await fetch(url)
            const data = await response.text()
        
            const document = new denodom.DOMParser().parseFromString(data, 'text/html')
        
            const result = document ? extractPairIdFromPage(document) : []
            result.map( v => output.add(v) )
        } )
    } )

    await Promise.all(workers)

    return [...output] as number[]
}

function extractPairIdFromPage(document: denodom.HTMLDocument) {
    const trs = document ? [...document.querySelectorAll("tr")] as denodom.Element[] : []
    const pairIds = trs ? [...trs].map(tr => {
        const idAttr = tr.getAttribute('id')?.toString()
        const pairId = idAttr?.slice(0, 5) === 'pair_' ? Number(idAttr?.slice(5)) : null
        return pairId
    }).filter(v => v !== null) : []

    return pairIds
}
