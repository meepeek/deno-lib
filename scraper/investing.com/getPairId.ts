import {SymbolInfoFormat} from './_type.ts'

export async function getPairId(
  pairId: number,
  sid = "8a7e26dbed121c6a9ef5585f1a848e37/1654835641/1/1/8",
) {
  const url =
    `https://tvc6.investing.com/${sid}/symbols?symbol=${pairId}`;
  const response = await fetch(url);
  const data: SymbolInfoFormat = await response.json();

  return data;
}

if (import.meta.main) await main();
async function main() {
  const pairId = Deno.args[0] ? Number(Deno.args[0]) : 169;

  const result = await getPairId(pairId);
  console.log(result);
}
