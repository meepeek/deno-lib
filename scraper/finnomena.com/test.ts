import {datetime} from '../deps.ts'

console.log(datetime.format(new Date('2022-06-10T00:00:00+07:00'), 'yyyy-MM-dd:hh:mm:ss'))
console.log(new Date('2022-06-10T00:00:00+00:00').toISOString())