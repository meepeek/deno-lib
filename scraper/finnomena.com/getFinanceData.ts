/**
 * 
 * @param finnomenaCode 
 * @returns [{}]
 */
export async function getFinanceData(finnomenaCode: number) {
    const url = `https://www.finnomena.com/fn3/api/stock/financial?securityID=${finnomenaCode}&fiscal=1990`
    const response = await fetch(url)
    const data = await response.json()

    return parse(data.data)

    function parse(data: { [x: string]: number; }[]) {
        return data.map((o: { [x: string]: number; }) => {
            const keys = Object.keys(o)
            const output = {} as {[key: string]: number}
            for (const key of keys) {
                const newKey = translatePropName(key)
                newKey ? output[newKey] = o[key] : output[key] = o[key]
            }
            return output
        })
    }
}

function translatePropName(name: string) {
    const lookupTable: {[x: string]: string | null} = {
        Fiscal: 'fiscal',
        Quarter: 'quarter',
        Asset: 'asset',
        BookValuePerShare: 'bookValuePerShare',
        Cash: 'cash',
        Close: 'close',
        DA: 'da',
        DebtToEquity: 'de',
        DividendYield: 'dividendYield',
        EVPerEbitDA: 'evPerEbitDA',
        EarningPerShare: 'eps',
        EarningPerShareQoQ: 'epsQoQ',
        EarningPerShareYoY: 'epsYoY',
        EbitDATTM: 'ebitDattm',
        Equity: 'equity',
        FinancingActivities: 'financingActivities',
        GPM: 'gpm',
        GrossProfit: 'grossProfit',
        InvestingActivities: 'investingActivities',
        MKTCap: 'marketCap',
        NPM: 'npm',
        NetProfit: 'netProfit',
        NetProfitQoQ: 'netProfitQoQ',
        NetProfitYoY: 'netProfitYoY',
        OperatingActivities: 'operatingActivities',
        PaidUpCapital: 'paidUpCapital',
        PriceBookValue: 'pbv',
        PriceEarningRatio: 'pe',
        ROA: 'roa',
        ROE: 'roe',
        Revenue: 'revenue',
        RevenueQoQ: 'revenueQoQ',
        RevenueYoY: 'revenueYoY',
        SGA: 'sga',
        SGAPerRevenue: 'sgaPerRevenue',
        TotalDebt: 'totalDebt'
    }
    return lookupTable[name]
}


if (import.meta.main) await main()
async function main() {
    const finnomenaCode = Deno.args[0] ? Number(Deno.args[0]) : 852
    const result = await getFinanceData(finnomenaCode).catch(e => console.log(e))
    console.log(result)
}
