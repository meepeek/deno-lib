import {convertDateToUnix} from '../_lib/lib.ts'
import {datetime} from '../deps.ts'
import {ClosePriceFormat} from './_type.ts'

type Fn = (symbol: string, unixFrom: number, unixTo: number) => Promise<ClosePriceFormat[]>

const finnomenaFn: {[key: string]: Fn } = {
    stock: async (symbol: string, unixFrom: number, unixTo: number) => {
        const url = `https://www.finnomena.com/fn3/api/tv/trade?symbol=${encodeURIComponent(symbol)}&resolution=D&from=${unixFrom}&to=${unixTo}`

        const response = await fetch(url)
        const data = await response.json()
        return parse(data) as ClosePriceFormat[]
    
        function parse(data: { [key: string]: number[] }) {
            if (!data) return []
            const {t, c, h, l, o, v} = data
            return t.map( (unix, i) => {
                const date = (new Date(unix * 1e3)).toISOString().split('T')[0]
                return {
                    date,
                    o: +o[i],
                    c: +c[i],
                    h: +h[i],
                    l: +l[i],
                    v: +v[i]
                }
            } )
        }
    },
    fund: async (symbol: string, unixFrom: number, unixTo: number) => {
        const url = `https://www.finnomena.com/fn3/api/fund/v2/public/tv/history?symbol=${encodeURIComponent(symbol)}&resolution=1D&from=${unixFrom}&to=${unixTo}&currencyCode=THB`
    
        const response = await fetch(url)
        const data = await response.json()
        return parse(data)
    
        function parse(data: { [key: string]: number[] }) {
            if (!data) return []
            const {t, c, v} = data
            return t.map( (unix, i) => {
                const date = (new Date(unix * 1e3)).toISOString().split('T')[0]
                return {
                    date,
                    c: +c[i],
                    v: +v[i]
                }
            } )
        }
    }
}

/**
 * 
 * @param symbol 
 * @param type 
 * @param from yyyy-MM-ddThh:mm:ss+07:00
 * @param to yyyy-MM-ddThh:mm:ss+07:00
 * @returns [{date, c, v, o?, h?, l?}]
 */
export const getClosePrices = async (symbol: string, type: string, from: string|null = null, to: string|null = null) => {
    // default values in case of null
    from = from ? from : (() => {
        const temp = new Date()
        temp.setDate(temp.getDate() - 30)
        return datetime.format(temp, 'yyyy-MM-dd')
    })()
    to = to ? to : datetime.format(new Date(), 'yyyy-MM-dd')

    const unixFrom = convertDateToUnix(new Date(from))
    const unixTo = convertDateToUnix(new Date(to))
    
    return await finnomenaFn[type](symbol, unixFrom, unixTo)
}

if (import.meta.main) await main()
async function main() {
    const symbol = Deno.args[0] ?  Deno.args[0].toUpperCase() : 'AP'
    const type =  Deno.args[1] ?  Deno.args[1] : 'stock'
    const result = await getClosePrices(symbol, type, '2000-01-01', '2022-07-04')
    console.log(result)
}
