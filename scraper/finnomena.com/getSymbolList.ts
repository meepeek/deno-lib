import {SymbolInfoFormat} from './_type.ts'

const stockList = async () => {
    const url = 'https://www.finnomena.com/fn3/api/stock/public/list'
    const response = await fetch(url);
    const {data} = await response.json();
  
    return parse(data) as SymbolInfoFormat[]
  
    // deno-lint-ignore no-explicit-any
    function parse(data: any) {
        // deno-lint-ignore no-explicit-any
        return data.map((o: any) => ({
            symbol: o.name,
            type: 'stock',
            nameTH: o.thName,
            nameEN: o.enName,
            finnomenaCode: o.security_id
        }))
    }
}

const fundList = async () => {
    const url = 'https://www.finnomena.com/fn3/api/fund/public/list'
    const response = await fetch(url);
    const data = await response.json();
  
    return parse(data) as SymbolInfoFormat[]

    // deno-lint-ignore no-explicit-any
    function parse(data: any) {
        // deno-lint-ignore no-explicit-any
        return data.map((o: any) => ({
            symbol: o.short_code,
            type: 'fund',
            nameTH: o.name_th,
            finnomenaCode: o.id
        }))
    }
}

export const getSymbolList = async () => {
    const list = [await fundList(), await stockList()]
    return list.flat()
}

if (import.meta.main) await main();
async function main() {
  const result = await getSymbolList();
  console.log(result);
}
