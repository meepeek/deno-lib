export type ClosePriceFormat = {
    date: string,
    o?: number,
    c: number,
    h?: number,
    l?: number,
    v: number
}

export type SymbolInfoFormat = {
    symbol: string,
    type: string,
    nameTH: string,
    nameEN?: string,
    finnomenaCode: string
}
