export type SymbolInfoFormat = {
  symbol: string;
  nameTH: string;
  nameEN: string;
  market: string;
  securityType: string;
  typeSequence: number;
  industry: string;
  sector: string;
  querySector: string;
  isIFF: boolean;
  isForeignListing: boolean;
  remark: string;
};

export const SecurityType: Record<string, string> = {
  S: "stock",
  P: "preferred-stock",
  W: "warrant",
  F: "foreign-invest",
  Q: "foreign-preferred-invest",
  U: "fund",
  L: "etf",
  X: "depositary-receipt",
  V: "future",
}

export type ClosePriceFormat = {
  date: string;
  symbol: string;
  prior: number | null;
  open: number | null;
  high: number | null;
  low: number | null;
  average: number | null;
  close: number | null;
  change: number | null;
  percentChange: number | null;
  totalVolume: number | null;
  totalValue: number | null;
  pe: number | null;
  pbv: number | null;
  bookValuePerShare: number | null;
  dividendYield: number | null;
  marketCap: number | null;
  listedShare: number | null;
  par: number | null;
  financialDate: string;
  nav: number | null;
  marketIndex: number | null;
  marketPercentChange: number | null;
};

export type CurrentPriceFormat = {
  datetime: string;
  price: number | null;
  volume: number | null;
  value: number | null;
};

export type FinanceInfoFormat = {
  symbol: string;
  quarter: string;
  year: number;
  fsType: string;
  beginDate: string;
  endDate: string;
  asOfDate: string;
  totalAsset: number;
  totalLiability: number;
  equity: number;
  paidupCapital: number;
  totalRevenue: number;
  totalExpense: number;
  sales: number;
  ebit: number;
  ebitda: number;
  netProfit: number;
  profitFromOtherActivity: number;
  eps: number;
  netOperating: number;
  netInvesting: number;
  netFinancing: number;
  netCashflow: number;
  roa: number;
  roe: number;
  netProfitMargin: number;
  grossProfitMargin: number;
  deRatio: number;
  currentRatio: number;
  quickRatio: number;
};
