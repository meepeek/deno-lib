import {ClosePriceFormat} from './_type.ts'

export async function getClosePrices(symbol: string) {
  const url = `https://www.set.or.th/api/set/stock/${symbol}/historical-trading`
  const response = await fetch(url);
  const data = await response.json();

  const prices: ClosePriceFormat[] = data
  return prices
}

if (import.meta.main) await main()
async function main() {
  const symbol = Deno.args[0] ? Deno.args[0].toUpperCase() : 'AP'

  const result = await getClosePrices(symbol)
  console.log(result)
}
