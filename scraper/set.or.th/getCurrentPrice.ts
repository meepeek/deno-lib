import {CurrentPriceFormat} from './_type.ts'

type XData = {
    prior: number | null,
    quotations: CurrentPriceFormat[]
}

export async function getCurrentPrice(symbol: string) {
    const url = `https://www.set.or.th/api/set/stock/${symbol}/chart-quotation?period=1D&accumulated=true`
    const response = await fetch(url);
    const data: XData = await response.json();

    const latest = data.quotations.filter((o: CurrentPriceFormat) => o.price !== null).slice(-1)[0]
    return latest
}

if (import.meta.main) await main()
async function main() {
    const symbol = Deno.args[0] ? Deno.args[0].toUpperCase() : 'AP'

    const result = await getCurrentPrice(symbol)
    console.log(result)
}
