import {FinanceInfoFormat} from './_type.ts'

export async function getFinanceData(symbol: string) {
    const url = `https://www.set.or.th/api/set/stock/${symbol}/company-highlight/financial-data`
    const response = await fetch(url);
    const data: FinanceInfoFormat[] = await response.json();

    return data
}

if (import.meta.main) await main()
async function main() {
    const symbol = Deno.args[0] ? Deno.args[0].toUpperCase() : 'AP'

    const result = await getFinanceData(symbol)
    console.log(result)
}
