import {SymbolInfoFormat} from './_type.ts'

export async function getSymbolList() {
  const url = "https://www.set.or.th/api/set/stock/list";
  const response = await fetch(url);
  const { securitySymbols: data } = await response.json();

  return data as SymbolInfoFormat[];
}

if (import.meta.main) await main();
async function main() {
  const result = await getSymbolList();

  console.log(result);
}
