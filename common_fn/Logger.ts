// deno-lint-ignore-file no-explicit-any
import { format } from "https://deno.land/std@0.145.0/datetime/mod.ts";
import { ensureDirSync } from "https://deno.land/std@0.145.0/fs/mod.ts"

export class Logger {
  name = "";
  dir_path: string | null = null;
  prefix_fn = (level: string) => {
    const date = format(new Date(), "yyyy-MM-dd HH:mm:ss");
    return `${this.name}|${level.toUpperCase()}|${date}:`;
  };

  constructor(name: string, dir_path?: string) {
    this.name = name;
    if (dir_path) this.dir_path = dir_path;
  }

  create_sub_logger(name: string) {
    const instance = new Logger(name);
    instance.dir_path = this.dir_path;
    instance.prefix_fn = this.prefix_fn;
    return instance
  }
  prefix(fn: () => string) {
    this.prefix_fn = fn;
  }
  log_dir(dir_path: string) {
    this.dir_path = dir_path;
  }
  info(...args: any[]) {
    this.write_log("info", ...args);
  }
  debug(...args: any[]) {
    this.write_log("debug", ...args);
  }
  warn(...args: any[]) {
    this.write_log("warn", ...args);
  }
  error(...args: any[]) {
    this.write_log("error", ...args);
  }

  private write_log(level: string, ...args: any) {
    const prefix = this.prefix_fn(level);
    const console_level: (...args: any[]) => void =
      console[level as keyof typeof console];
    console_level(prefix, ...args);
    if (this.dir_path) {
        const date_month = format(new Date(), 'yyyy-MM')
        ensureDirSync(this.dir_path)
      Deno.writeTextFile(
        `${this.dir_path}/${this.name}_${date_month}.txt`,
        `${prefix} ${args
            .filter((arg: any) => typeof arg !== "function")
            .map((arg: any) => (typeof arg === 'object') ? JSON.stringify(arg) : arg)
        }\n\r`,
        {append: true, create: true}
      );
    }
  }
}
